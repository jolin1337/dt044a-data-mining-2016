<center> <h1>Laboration 3: Model Creation and Validation</h1> </center>
<center><h2>DT044A Data Mining<h2></center>
<center><h2>Mid Sweden University<h2></center>

In lecture 3, the importance of sound model creation and validation has been raised. In this laboration, you will collect practical experience on the sound creation and validation of basic classifiers. There is no right or wrong order in which to do the exercises. None of the results are supposed to be submitted.

### 1. Validation Questions using Weka

Open the segment-challenge dataset and go to the Classify tab.

A. Select the J48 classifier (default options), select Percentage split as test option, and determine the proportion of correctly classified instances when the following percentages are used for training set size: 10%, 20%, 40%. 60%, 80%. What do you observe?

1. There are some fluctuations, but performance generally increases with training set size.
2. Performance stays about the same.
3. Performance always increases as training set size increases.
4. Performance tends to decrease as training set size increases.

B. Repeat Question A using the training set percentages of 90%, 95%, 98%, and 99%. What happens to the number of correctly classified instances?

1. Stays about the same.
2. Increases slowly.
3. Decreases a little bit.
4. Decreases dramatically.

C. Repeating Question A with a training set percentage of 99% gives a figure of 100% accuracy on the test set. Does this mean that this generates a perfect classifier?

1. Yes
2. No

D. Based on the above experiments, what is your best estimate of J48's true accuracy on the segment-challenge dataset?

1. 50%
2. 90%
3. 95%
4. 100%

E. What is the likelihood that J48 would make no mistakes on 15 independently chosen test instances, if its accuracy for each instance was 95%?

1. Very unlikely
2. About 50%
3. Almost 100%

F. Which of the following statements seems to be correct, based on your experiments?

1. The more training data, the greater the classifier's success rate.
2. Training set size has no influence on the classifier's success rate.
3. The more test data, the greater the classifier's success rate.

Open the diabetes dataset.

G. When the percentage split option is used for evaluation, how good is the performance if (a) almost none of the data is used for testing; (b) almost all of the data is used for testing?

1. (a) has poor performance, (b) has good performance.
2. (a) has better performance than (b).
3. The performance for (a) and (b) are similar.

H. Select Percentage split as test option and set percentage for training to 80%. How many instances will be used for training, and how many for testing?

1. 614 and 154.
2. 615 and 153.
3. 691 and 77.
4. 1211 and 289 for training and testing respectively.

I. What are the minimum and maximum values for the number of incorrectly classified instances?

1. 20 and 29.
2. 31 and 44.
3. 71 and 80.
4. 110 and 123.

J. What is the mean of the accuracy for these five seed values?
 
K. What is the standard deviation of the accuracy for these five seed values?

L. If you did the above experiment with 10 different random seeds rather than 5, how would you expect this to affect the mean and standard deviation?

1. They would both stay about the same.
2. The mean would be a bit bigger but the standard deviation would be about the same.
3. The mean would be about the same and the standard deviation would be a little smaller.
4. Both the mean and standard deviation would be a bit smaller.

IF YOU HAVE JUST COMPLETED THE LESSON BEFORE, PLEASE EITHER RESTART WEKA OR BE SURE TO INITIALIZE THE RANDOM SEED TO ITS DEFAULT VALUE OF 1.

M. The iris dataset consists of three classes (Iris-setosa, Iris-versicolor, Iris-virginica), with 50 instances each. What is the accuracy of ZeroR on this dataset when testing on the training set?

1. 10%
2. 33%
3. 50%
4. 66%

N. In practice, what is ZeroR's success rate on the iris dataset when evaluated using the default (66%) percentage split?

O. What is ZeroR's accuracy (in percent)?

P. What is J48's accuracy (in percent), using default parameter values?

Q. What is NaiveBayes' accuracy (in percent), using default parameter values?

R. What is ZeroR's accuracy (in percent)?
  
S. What is IBk's accuracy (in percent), using default parameter values?

T. What is PART's accuracy (in percent), using default parameter values?

U. Open the iris dataset and evaluate the accuracy of the baseline ZeroR method using cross-validation with 10, 11, 12, 13, 14, and 15 folds. What is the minimum and maximum value of the six results obtained?

1. They're all 33%
2. They're all 50%
3. 28% and 35%
4. 28% and 33%

V. It's curious that all values obtained in the previous question were less than or equal to ZeroR's true accuracy value of 33% on this dataset. Is this a coincidence?

1. Yes
2. No

W. Suppose the accuracy of ZeroR on the iris dataset were evaluated using cross-validation with 5, 10, and 25 folds. Without doing the experiment, what would you expect the accuracies to be?

1. All exactly 33%
2. All around about 33%, some bigger, some smaller
3. All 33% or perhaps a little bit smaller

X. What would ZeroR's success rate on the iris datset be if evaluated using 150-fold cross-validation? Think carefully about this first, and then confirm your answer using Weka.

1. 0%
2. 29%
3. 33%
4.  50%
5. 66%

Open the segment-challenge dataset and go to the Classify tab.

Select the J48 classifier (default options), select Cross-validation as the test option and set use 10 folds. Evaluate J48 with the following seed values:
11, 12, 13, 14, 15

Y. What is the mean of the accuracy (in percent; round accuracies to one decimal place)?

Z. What is the standard deviation of the accuracy (in percent; round accuracies to one decimal place)?

AA. When you performed the above experiment, how many times did Weka run the J48 algorithm?

1. 5
2. 6
3. 50
4. 55
5. 60

AB. What is the mean of the accuracy (in percent; round accuracies to one decimal place)?

AC. What is the standard deviation of the accuracy?

AD. When you performed the above experiment, how many times did Weka run the J48 algorithm?

1. 5
2. 6
3. 10
4. 50
5. 55
6. 60

Open the iris dataset and go to the Classify tab.

AE. Perform 10-fold cross-validation using ZeroR and OneR. Which classifier achieves higher accuracy?

1. OneR
2. ZeroR
3. Both have the same accuracy

AF. Which attribute does OneR use for generating the rule in the previous experiment, when using the full dataset?

1. sepallength
2. sepalwidth
3. petallength
4. petalwidth
5. class

AG. Could there be a dataset for which ZeroR outperforms OneR?

1. No. OneR always outperforms ZeroR.
2. Yes. ZeroR sometimes outperforms OneR.

AH. Could there be a dataset for which ZeroR outperforms OneR when evaluated on the training data?

1. No
2. Yes

AI. Inspect the data using the Edit button of Weka's Preprocess panel. What is the maximum accuracy of rules based on temperature and humidity respectively, in terms of the number of training instances predicted correctly?

1. 12 for temperature, 10 for humidity
2. 13 for temperature, 12 for humidity
3. 14 for temperature, 14 for humidity

-------------------------------------------------------------------

The following questions investigate the effect of OneR's minBucketSize parameter on performance and rule complexity by drawing graphs where -B/minBucketSize ranges from 1 to 10.

AJ. Open the glass dataset, go to the Classify tab, and select OneR. Make a rough paper-and-pencil plot of accuracy on the training data (on the y axis) against minBucketSize (on the x axis) and compare it with the graphs below. Which of these shapes do you get?

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/OneRBucket.png" alt="OneR Buckets" style="width:400px"/>
</center>

AK. Now consider the complexity of the rule that OneR generates, measured by its size -- the number of tests it involves. Will the rule's complexity depend on whether the training set or cross-validation is used for evaluation?

1. Yes
2. No

AL. Using paper and pencil, plot the size of the rule generated against minBucketSize. Which of these plots do you get?

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/OneRBucketCrossValidation.png" alt="OneR Buckets Cross Validation" style="width:400px"/>
</center>

-------------------------------------------------------------------

Open the vote dataset and select the NaiveBayes classifier, with default options and 10-fold cross-validation as the evaluation method.

AM. What is NaiveBayes's accuracy on this dataset?

AN. Return to the Preprocess tab and copy the first attribute, handicapped-infants, ten times, using the Copy filter. (You will run into a problem that is easily solved with a little ingenuity. And don't forget that the word last can be used as an attribute index.) What is NaiveBayes's accuracy on this new dataset, again evaluated using 10-fold cross-validation?

AO. Return to the Preprocess tab and copy the same attribute a further ten times. What is the accuracy now?

  
AP. You're probably thinking that if you were to continue to make copies of the handicapped-infants attribute and evaluate by 10-fold cross-validation, the accuracy would gradually decrease until it finally leveled off. And you'd be right! What percentage accuracy does it level off at? (Hint: do not make more copies of this attribute. Use what you know about Naive Bayes to figure out a better, quicker, and more reliable way.)

1. 68.4%
2. 68.5%
3. 68.6%
4. 68.7%

AQ. If Naive Bayes's accuracy continually deteriorates as you add copies of a particular attribute (as it does here for handicapped-infants), do you think it would improve if that attribute was completedly removed from the dataset?

1. Yes
2. No
3. Maybe

### 2. Soccer Data Analysis

Start your Postgres database using pgAdmin (or the command line if that suits you better).

1) Using a select statement, create a data-frame that contains the following columns for all games in the database:

* position of home team in league table before the game
* position of away team in league table before the game
* the difference in position between the teams before the game
* *outcome* as nominal attribute from the view of the home team WIN-DRAW-LOSS

Once you have done that, export the data-frame through the menu bar "File -> Export" by choosing comma as separator to a file of your choice.
Now, import the file into Weka as with any other data-set.

3) Assume that you want to build a classifier on the data-set to predict the *outcome*.

* What would be an appropriate way of separating training and test data for the data-set?
* Which is the best classifier you can build for the data?
* How do you validate/evaluate that it is best?
* How important is the use of a cost function in this scenario?

4) Can you extend the data-set with further interesting attributes to reduce the classification error?

<p align="right">*Felix Dobslaw* - felix.dobslaw@miun.se</p>
