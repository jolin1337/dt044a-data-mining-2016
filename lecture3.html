<!DOCTYPE html>
<html>
  <head>
    <title>DT044A Data Mining, Introduction</title>
    <meta charset="utf-8">
    <style>
      @import url(https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz);
      @import url(https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic);
      @import url(https://fonts.googleapis.com/css?family=Ubuntu+Mono:400,700,400italic);

      body { font-family: 'Droid Serif'; }
      h1, h2, h3 {
        font-family: 'Yanone Kaffeesatz';
        font-weight: normal;
      }
      .remark-code, .remark-inline-code { font-family: 'Ubuntu Mono'; }
    </style>
  </head>
  <body>

<textarea id="source">

class: center, middle, inverse

##DT044A, Data Mining 7.5 hp

Lecture 3: Concept Design Evaluation

Felix Dobslaw

Mid Sweden University

2016-02-04
---

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/CRISP.png" alt="CRISP lectures" style="width: 750px" align="middle"/>
</center>

---

# Lecture Content <img src="https://dl.dropboxusercontent.com/u/35363454/datamining/output.png" alt="output" style="width:50px"/>

1. Model Creation <img src="https://dl.dropboxusercontent.com/u/35363454/datamining/output.png" alt="output" style="width:12px"/>
	* Model Selection
2. Model Evaluation/Validation <img src="https://dl.dropboxusercontent.com/u/35363454/datamining/output.png" alt="output" style="width:12px"/>
	* Comparison
	* Prediction Error
	* Prediction Error Costs

---

<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/evaluationOverview.png" alt="evaluation overview" style="width:800px"/>

---

# Model Selection

* Why model X?
	* Why configuration Y?

E.g. X = K-nearest neighbors, Y = Choice of K

### Theory:
* *"all algorithms are equivalent, on average..."* (No Free Lunch Theorem, Wolpert in 1996)

### Practice:
* Certain algorithms are (empirically) better for certain problem sets. Much of todays research in computer sciences presents that algorithm A is better than algorithm B on a class of problems C. **How to evaluate this???**

---

# Accuracy and Error

Let us assume the following output of a learned classifier:

```
=== Summary ===

Correctly Classified Instances (S)       10               71.4286 %
Incorrectly Classified Instances (F)      4               28.5714 %
```

* **How accurate are those $71.4286\%$?**
	* $\Rightarrow$ depends on the number of instances in training data, N

We usually talk about confidence intervals with a standard deviation $\sigma$, so that $71.4286\% \pm \sigma$.

How to calculate $\sigma$?

---

# Confidence Intervals

* For Bernoulli trial experiments with S number of success, and $f=\frac{S}{N}$ being the success rate.
* Standard deviation is $\frac{p(1-p)}{N}$.

For large enough N, f follows normal distribution.
* confidence interval $[-z,z]$ for random variable with 0 mean is given by:
$$P(z\leq X\leq z) = c$$

For the symmetric case: $c = 1 - 2 \times P(X > z)$

Transformation to $\mathcal{N}(0,1)$ (Normal Distribution with $\mu$=$0$ and $\sigma$=$1$)

$$P(-z \leq \frac{f-p}{\sqrt{p(1-p)/N}} \leq z)=c$$

---
# Confidence Limits $\mathcal{N}(0,1)$

<style>
      .left-column {
        width: 50%;
        float: left;
      }
      .right-column {
        width: 50%;
        float: right;
      }
</style>

.left-column[

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-0ord{text-align:right}
.tg .tg-lqy6{text-align:right;vertical-align:top}
.tg .tg-hgcj{font-weight:bold;text-align:center}
</style>
<table class="tg">
  <tr>
    <th class="tg-hgcj">$P(X \leq z)$ (%)</th>
    <th class="tg-hgcj">z</th>
  </tr>
  <tr>
    <td class="tg-0ord">0.1</td>
    <td class="tg-0ord">3.09</td>
  </tr>
  <tr>
    <td class="tg-0ord">0.5</td>
    <td class="tg-0ord">2.58</td>
  </tr>
  <tr>
    <td class="tg-lqy6">1</td>
    <td class="tg-lqy6">2.33</td>
  </tr>
  <tr>
    <td class="tg-lqy6">5</td>
    <td class="tg-lqy6">1.65</td>
  </tr>
  <tr>
    <td class="tg-lqy6">10</td>
    <td class="tg-lqy6">1.28</td>
  </tr>
  <tr>
    <td class="tg-lqy6">20</td>
    <td class="tg-lqy6">0.84</td>
  </tr>
  <tr>
    <td class="tg-lqy6">40</td>
    <td class="tg-lqy6">0.25</td>
  </tr>
</table>

* To find the interval we must solve:
$$p=\frac{f+\frac{z^2}{2N}\pm z\sqrt{\frac{f}{N}-\frac{f^2}{N}+\frac{z^2}{4N^2}}}{1+\frac{z^2}{N}}$$
]

.right-column[

**Example:**

* Given: $f = 75\%$
* We decide confidence: $P(X \leq z) = 10\%$
* Because of $\mathcal{N}(0,1) \Rightarrow z = 1.28$
* $\Rightarrow P(-1.28 \leq X \leq 1.28) = 80\%$
* Putting the values into formula:
	 * $N = 10 \Rightarrow [0.549, 0.881]$
	 * $N = 100 \Rightarrow [0.691, 0.801]$
	 * $N = 1000 \Rightarrow [0.732, 0.767]$
]

---

# Overfitting

<center>
<img src="https://shapeofdata.files.wordpress.com/2013/02/overfitting.png" alt="overfitting" style="width:650px"/>
</center>

Underfit vs. quite right vs. Overfit

---

# quite right, OneR

Example on weather dataset (min bucket size 6):

```
temperature:
	< 77.5	-> yes
	>= 77.5	-> no
(9/14 instances correct)


Time taken to build model: 0 seconds

=== Summary ===

Correctly Classified Instances           9               64.2857 %
Incorrectly Classified Instances         5               35.7143 %

=== Confusion Matrix ===

 a b   <-- classified as
 2 3 | a = no
 2 7 | b = yes

```

---

# Overfitting OneR

Example on weather dataset (min bucket size 1):

```
temperature:
	< 64.5	-> yes
	< 66.5	-> no
	< 70.5	-> yes
	< 73.5	-> no
	< 77.5	-> yes
	< 80.5	-> no
	< 84.0	-> yes
	>= 84.0	-> no
(13/14 instances correct)

=== Summary ===

Correctly Classified Instances          13               92.8571 %
Incorrectly Classified Instances         1                7.1429 %

=== Confusion Matrix ===

 a b   <-- classified as
 5 0 | a = no
 1 8 | b = yes
```
---
# Wisdom of the Crowd

* crowds tend to have good judgement/ better than individuals

Let's make an experiment; guess a quantity!
* independent $\Rightarrow$ unbiased

Calculate mean and variance:
$$\mu = \frac{\sum x_i}{n}$$
$$\\sigma^2 = \frac{\sum (x_i-\mu)^2}{n-1}$$

What are $\mu$ and $\sigma$?

---


# Model Creation: Infinite Population

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/validation.png" alt="validation" style="width:650px"/>
</center>

---


# Model Creation: Limited Population

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/validationRealWorld.png" alt="validation" style="width:650px"/>
</center>

---


# Training vs. Test

**Resubstitution Error** = Error rate on training data
$\Rightarrow$ does not generalize

Houldout procedure: Splitting original data into **training** and **test** data

Three DISTINCT data-sets:
* Training
* Validation (fine-tuning)
* Testing

1. Train model(s)
	* optional: fine-tune model(s)
2. Test model(s) $\Rightarrow$ **Valid Error Rates!**

General:
* **larger** training data $\Rightarrow$ **better** classifier
* **larger** test data $\Rightarrow$ **more accurate** error estimate
---

# Model Validation Methods

Problem: *Few instances*

**Approaches:**

1. Houldout (e.g. 70% - 30%)
2. Cross-Validation (e.g. 10-fold)
3. Stratified Cross-Validation

* All can be used with **repeated** splitting.
* Repeated Stratified Cross-validation to be preferred in most situations.

---

# Holdout

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/holdout.png" alt="holdout" style="width:450px"/>
</center>

Potential problem: sample not representative
* class might be missing in test data
* unbalanced splitting $\Rightarrow$ inaccurate error rate

Solution:

**Stratification**, each class represented with approximately equal proportion in all subsets.

---

# Cross-Validation

* decide number n of (approximately) equally sized splits
* n rounds where n-1 parts are used for training, and one for testing
* In Weka for 10-fold:
	* Weka runs experiment 11 times
	* accuracy based on 10 runs
	* training on entire data set because (larger <img src="https://dl.dropboxusercontent.com/u/35363454/datamining/input.png" alt="input" style="width:12px"/> $\Rightarrow$ better <img src="https://dl.dropboxusercontent.com/u/35363454/datamining/output.png" alt="output" style="width:12px"/>)
* Special case: leave one out method where n=N
	* no random sub-sampling
	* computationally expensive (-)
	* no stratification (-)

* All samples used for training and testing (+)
* Empirical and theoretical evidence for superiority of method (+)
---

# n-fold Cross-Validation

<center>
<img src="http://cse3521.artifice.cc/images/k-fold-cross-validation.jpg" alt="k-fold cross-valudation" style="width:650px"/>
</center>

---

# Stratified Cross-Validation

* Make sure to uniformly distribute class occurence among all splits.

**Question:**
* Imagine you are supposed to do SCV by hand. How could it be realized?
* What are three possible splits for the data set?

Data set for 3-fold SCV:

```
id			class
1			ONE
2			ONE
3			TWO
4			THREE
5			THREE
6			TWO
7			TWO
8			TWO
9			TWO
10   		THREE
```


---

# Random Seeds

Holdout is based on random choice of instances
* Choice of random seed
	* effects choice of instances
	* allows for repeatable results

---

# Holdout vs. Cross Validation

```
Holdout (10%)	Cross-Validation (10-fold)
75.3			73.8
77.9			75.0
80.5			75.5
74.0			75.5
71.4			74.4
70.1			75.6
79.2			73.6
71.4			74.0
80.5			74.5
67.5			73.0
```
Holdout: $\mu = 74.8, \sigma = 4.6$

Cross-Validation: $\mu = 74.5, \sigma = 0.9$

---

# Further Model Validation Methods

If you have **very** small data sets.

**Notation:** $|\cdot|$ operator counts the number of instances.

* Leave one out Cross-Validation
* Bootstrap
	* sampling N times with repetition
	* $\Rightarrow |Data| = |Training| \wedge Test = Data \setminus Training $


---
# Bootstrap Properties

$$ P(d \in Test| d \in Data) = (1-\frac{1}{N})^N$$
$$P(d \in Training| d \in Data) = (\frac{1}{N})^N$$
$$\lim\limits_{N \to \infty} P(d \in Training| d \in Data) \rightarrow 0.368$$

* training data contains  approximately 63.2% of instances.

$$Error = 0.638 \times ErrorTest + 0.368 \times ErrorTraining$$

* requires repetition (mean and variance calculation as above)
---

# Bootstrap Example

* Data set : $\\{a,b,c,d,e\\}$

Round 1:  $Training = \\{c,a,e,c,a\\} \Rightarrow Test = \\{b,d\\}$

Round 2:  $Training = \\{a,c,d,d,e\\} \Rightarrow Test = \\{b\\}$

Aggregate result: Voting scheme (classification) or average (regression)

---

# Which Classifier is (significantly) better?

Is scheme A better than B? (usually dateset specific)
* Always compare to a **baseline classifier** to see how hard the problem is


### Solution: Paired T-test

Are the means of cross-validation estimates significantly different at confidence level c?

If testing many schemes, t-test error accumulates, use ANOVA instead (not part of course, or Weka)

Using the Weka Experimenter, we get:
```
Dataset                   (1) trees.J48 '-C 0 | (2) rules.ZeroR
---------------------------------------------------------------
iris                     (1000   94.91( 5.43) |   33.33(0.00) *
labor-neg-data           (1000   79.80(14.95) |   64.67(3.06) *
---------------------------------------------------------------
                                      (v/ /*) |         (0/0/2)
```

---

# Parameter Setting

* Selecting and testing manually is tedious work
* There are tools that semi-automate, but some things you must always define:
	* data set
	* algorithm
	* degrees of freedom (e.g. bucket size OneR)
	* evaluation method (e.g. cross-validation to maximize accuracy)

One approach:

* Consider each new setting $Y$ for a model $X$ as a new model $X_Y$ (can be done in the Weka Experimenter)

**However:**
If you are training too much on little data, you overfit!

**Also:**
When comparing: be fair!! $\Rightarrow$ same budget.

---

# Numeric Prediction

**To be predicted:** Actual target values $a_1,...,a_n$

**Predicted values:** $p_1,...,p_n$

* $\mu_T$ is average of actual values training data
* $\mu_t$ is average of actual values test data
* $\mu_p$ is average of predicted values

---

# Commonly used

### Absolute

1. $MSE = \frac{1}{n}\sum\limits_i (p_i - a_i)^2$ (mean square error)
2. $RMSE = \sqrt{MSE}$ (root mean square error)
3. $MAE = \frac{1}{n}\sum\limits_i |p_i - a_i|$ (mean absolute error)

### Relative

1. $RSE = \frac{\sum\limits_i (p_i - a_i)^2}{\sum\limits_i (a_i - \mu_T )^2}$ (**relative** square error)
2. $RESE = \sqrt{RSE}$ (root **relative** square error)
3. $RAE = \frac{\sum\limits_i |p_i - a_i|}{\sum\limits_i |a_i - \mu_T|}$ (mean **relative** error)

---

# Correlation Coefficient

*Statistical correlation between actual and predicted values:*

$$CC = \frac{Spa}{\sqrt{Sa \times Sa}}, CC \in [-1,1]$$

$$Sp = \frac{1}{n-1} \sum\limits_i(p_i - \mu_p)$$
$$Sa = \frac{1}{n-1} \sum\limits_i(p_i - \mu_t)$$
$$Spa = \frac{1}{n-1} \sum\limits_i [ (a_i - \mu_t) \times (p_i - \mu_p) ] $$

The closer to 1, the better.

---

# Numeric Prediction

Choice of measure not *that* important. They often point into same direction.

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/numericErrorMeasure.png" alt="numeric error measures" style="width:750px"/>
</center>

---

# Success Measured in Probabilities

* some classifiers express *assessed* belongingness to class $c$ as probability $p_c$
* training data defines if instance belongs to c: $a_c \in\\{0,1\\}$ 

**Example for iris dataset:**
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-9hbo{font-weight:bold;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-9hbo">lense type</th>
    <th class="tg-031e">hard</th>
    <th class="tg-yw4l">soft</th>
    <th class="tg-yw4l">no</th>
  </tr>
  <tr>
    <td class="tg-9hbo">actual $a_c$</td>
    <td class="tg-yw4l">1</td>
    <td class="tg-yw4l">0</td>
    <td class="tg-yw4l">0</td>
  </tr>
  <tr>
    <td class="tg-9hbo">predicted $p_c$</td>
    <td class="tg-yw4l">0.6</td>
    <td class="tg-yw4l">0.3</td>
    <td class="tg-yw4l">0.1</td>
  </tr>
</table>

**BUT:** How good is that prediction?
* $\Rightarrow$ Loss Functions measure goodness of probabilistic classification

---

# Loss Functions

### Quadratic Loss (minimize)

$$QL = \sum\limits_c (p_c - a_c)^2 = 1 - 2a_cp_c + \sum\limits_c p_i^2, QL \in [0,2]$$

* considers all classes (+)

### Information Loss (minimize)

$$IL = - \log{p_c}, IL \in [0,\infty]$$

* simple (+)
* considers only the correct class (-)
* zero-frequency problem (-)
---

# Kappa Statistic

<style>
      .left-column {
        width: 60%;
        float: left;
      }
      .right-column {
        width: 40%;
        float: right;
      }

</style>
.left-column[
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-3i0c{font-weight:bold;background-color:#ffce93}
.tg .tg-uhkr{background-color:#ffce93}
.tg .tg-e3zv{font-weight:bold}
.tg .tg-gr78{background-color:#ffce93;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-3i0c">J48</th>
    <th class="tg-uhkr"></th>
    <th class="tg-3i0c" colspan="4">Predicted Class</th>
  </tr>
  <tr>
    <td class="tg-uhkr"></td>
    <td class="tg-uhkr"></td>
    <td class="tg-uhkr">a</td>
    <td class="tg-uhkr">b</td>
    <td class="tg-uhkr">c</td>
    <td class="tg-gr78">total</td>
  </tr>
  <tr>
    <td class="tg-3i0c" rowspan="3">Actual<br>Class</td>
    <td class="tg-uhkr">a</td>
    <td class="tg-e3zv">88</td>
    <td class="tg-031e">10</td>
    <td class="tg-031e">2</td>
    <td class="tg-yw4l">100</td>
  </tr>
  <tr>
    <td class="tg-uhkr">b</td>
    <td class="tg-031e">14</td>
    <td class="tg-e3zv">40</td>
    <td class="tg-031e">6</td>
    <td class="tg-yw4l">60</td>
  </tr>
  <tr>
    <td class="tg-uhkr">c</td>
    <td class="tg-031e">18</td>
    <td class="tg-031e">10</td>
    <td class="tg-e3zv">12</td>
    <td class="tg-yw4l">40</td>
  </tr>
  <tr>
    <td class="tg-gr78"></td>
    <td class="tg-gr78">total</td>
    <td class="tg-yw4l">120</td>
    <td class="tg-yw4l">60</td>
    <td class="tg-yw4l">20</td>
    <td class="tg-yw4l"></td>
  </tr>
</table>

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}
.tg .tg-3i0c{font-weight:bold;background-color:#ffce93}
.tg .tg-uhkr{background-color:#ffce93}
.tg .tg-e3zv{font-weight:bold}
.tg .tg-gr78{background-color:#ffce93;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-3i0c">random</th>
    <th class="tg-uhkr"></th>
    <th class="tg-3i0c" colspan="4">Predicted Class</th>
  </tr>
  <tr>
    <td class="tg-uhkr"></td>
    <td class="tg-uhkr"></td>
    <td class="tg-uhkr">a</td>
    <td class="tg-uhkr">b</td>
    <td class="tg-uhkr">c</td>
    <td class="tg-gr78">total</td>
  </tr>
  <tr>
    <td class="tg-3i0c" rowspan="3">Actual<br>Class</td>
    <td class="tg-uhkr">a</td>
    <td class="tg-e3zv">60</td>
    <td class="tg-031e">30</td>
    <td class="tg-031e">10</td>
    <td class="tg-yw4l">100</td>
  </tr>
  <tr>
    <td class="tg-uhkr">b</td>
    <td class="tg-031e">36</td>
    <td class="tg-e3zv">18</td>
    <td class="tg-031e">6</td>
    <td class="tg-yw4l">60</td>
  </tr>
  <tr>
    <td class="tg-uhkr">c</td>
    <td class="tg-031e">24</td>
    <td class="tg-031e">12</td>
    <td class="tg-e3zv">4</td>
    <td class="tg-yw4l">40</td>
  </tr>
  <tr>
    <td class="tg-gr78"></td>
    <td class="tg-gr78">total</td>
    <td class="tg-yw4l">120</td>
    <td class="tg-yw4l">60</td>
    <td class="tg-yw4l">20</td>
    <td class="tg-yw4l"></td>
  </tr>
</table>
]

.right-column[
* Table 1: success rate
	* $\frac{140}{200}=70\%$
* Table 2: random prediction based on same rate as Table 1
	* Success 82
* Kappa statistics:
	* $\kappa = \frac{140-82}{200-82} = 49.2\%$

$\kappa \in [0,100]$, the higher the better (0 is random).
]

---
# Prediction Error Cost

What if accuracy is **not** sufficient for the classifier.

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/confusionMatrix.png" alt="confusion matrix" style="width:600px"/>
</center>

**Example:** Imagine the situation of medical treatment:
* Give someone a preparate who does not need it (False Positive)
* Do not give someone a preparate who needs it (False Negative)

Depends on the problem at hand.

* How about predicting game result scenario (WIN, DRAW, LOSS)? Difference? 


---

# Cost Sensitivity

* Either consider in training or testing phase
 * **Training:** Duplicate instances to increase weight of high cost classes (Cost Sensitive Learning)
 * **Test:** Weight instances according to customized cost (Cost Sensitive Classification)

---

# Cost Sensitive Classification

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/costsensitive.png" alt="cost sensitive" style="width:750px"/>
</center>

---

# Lift Chart

* Helps in selecting a sub-sample of the data set with better fit than the whole set.

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/lift.png" alt="lift chart" style="width:650px"/>
</center>

---
# Lift Data

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/liftData.png" alt="lift chart data" style="width:400px"/>
</center>

---
# Receiver Operating Characteristics (ROC) Curve


x-axis: $\frac{FP}{TN+FP}$ (false positive rate)

y-axis: $\frac{TP}{TP+FN}$ (true positive rate)

calculate ROC curve by Cross-Validation:

1. collect probabilities for instances in test-folds
2. sort instances according to probability

---
# ROC Curve

<center>
<img src="https://upload.wikimedia.org/wikipedia/commons/3/36/ROC_space-2.png" alt="roc curve " style="width:450px"/>
</center>

---

# ROC Curve Example

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/rocExample.png" alt="roc curve example" style="width:650px"/>
</center>

---

# ROC Curve: Comparing Classifiers

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/roc.png" alt="roc curves" style="width:450px"/>
</center>

* Little Data $\Rightarrow$ use A, Else B
* Can be combined to cover the whole greyed spectrum (in book, but not part of course)	
---

# Precision Recall

<style>
      .left-column {
        width: 50%;
        float: left;
      }
      .right-column {
        width: 50%;
        float: right;
      }

</style>
.left-column[
<center>
<img src="https://upload.wikimedia.org/wikipedia/commons/2/26/Precisionrecall.svg" alt="Precision Recall" style="width:250px"/>
</center>

]

.right-column[
Relevant Items:
$$Precision = \frac{TP}{TP+FP}$$

Relevant Selected Items:
$$Recall = \frac{TP}{TP+FN}$$

Heavily used in Pattern Recognition and Information Retrieval
]
---
# Other Measures

* 3-point average recall (20, 50, 80%)
* 11-point average recall (0, 10, ..., 90, 100%)
* F-measure $\frac{2\times TP}{2\times TP + FP + FN}$
* Sensitivity $\times$ Specificity $\frac{TP \times TN}{(TP+FN)\times (FP + TN)}$
* Success Rate $\frac{TP + TN}{TP + TN + FP + FN}$

...

It is not important to know all these measures. When you meet problems of different kind, you can look at what measures are commonly used.
---

# Roundup

* Avoid Overfitting
	* use repeated Cross-Validation
	* split data into training-validation-test
	* get some practical experience in order to understand the consequences
* include costs to weigh consequences where appropriate
	* either training or testing

</textarea>

    <script src="http://gnab.github.io/remark/downloads/remark-latest.min.js" type="text/javascript"></script>
    <script src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML&delayStartupUntil=configured" type="text/javascript"></script>
    <script type="text/javascript">
	  var slideshow = remark.create({
	  // Set the slideshow display ratio
	  // Default: '4:3'
	  // Alternatives: '16:9', ...
	  ratio: '4:3',

	  // Navigation options
	  navigation: {
	    // Enable or disable navigating using scroll
	    // Default: true
	    // Alternatives: false
	    scroll: true,

	    // Enable or disable navigation using touch
	    // Default: true
	    // Alternatives: false
	    touch: true,

	    // Enable or disable navigation using click
	    // Default: false
	    // Alternatives: true
	    click: false
	  },

	  // Customize slide number label, either using a format string..
	  //slideNumberFormat: 'Slide %current% of %total%',
	  // .. or by using a format function
	  slideNumberFormat: function (current, total) {
	    return 'DT044A, Data Mining Lecture 3: Evaluation, Felix Dobslaw ' + current + '/' + total;
	  },

	  // Enable or disable counting of incremental slides in the slide counting
	  countIncrementalSlides: true
	}); 

      // Setup MathJax
      MathJax.Hub.Config({
          tex2jax: {
          skipTags: ['script', 'noscript', 'style', 'textarea', 'pre'],
	  inlineMath: [ ['$','$'], ["\\(","\\)"] ],
	  processEscapes: true
	  }
      });
      MathJax.Hub.Queue(function() {
          $(MathJax.Hub.getAllJax()).map(function(index, elem) {
              return(elem.SourceElement());
          }).parent().addClass('has-jax');
      });

      MathJax.Hub.Configured();
    </script>
  </body>
</html>
