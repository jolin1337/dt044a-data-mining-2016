<center> <h1>Laboration 3: Weka Answers</h1> </center>
<center><h2>DT044A Data Mining<h2></center>
<center><h2>Mid Sweden University<h2></center>

### Answers to Weka Questions

A. You would expect this general trend but with some fluctuation, but in this case performance always increases.

B. The numbers of correctly classified instances are 145, 72, 29, and 15 respectively. Although these numbers are decreasing, the percentage of correctly classified instances is increasing.

C. Although the classifier is indeed 100% correct on the test set, that set only contains 15 instances (1% of the 1500 instances in the data set). This is not a reliable predictor of performance on independent test data.

D. 95. In fact, 96% might be a better estimate.

E. In fact, the probability of getting 15 independent decisions correct is 0.95*0.95*0.95*0.95*... [15 times], which is 0.95**15 = 0.46, or 46%.

F. 1.

G. Leaving almost no data for testing means more data for training, so the algorithm has seen most patterns in the data and performs quite well on the remaining little test data.

H. The diabetes dataset has 768 instances, and an 80 : 20 split gives 614.4 : 153.6. Weka doesn't truncate, it rounds to the nearest integer.

I. The number of incorrect instances for the five seed values are 37, 35, 38, 31 and 44.

J. The random seeds generate the following accuracies: 76.0, 77.3, 75.3, 79.9, 71.4. These have a mean of 76.0%.

K. The random seeds generate the following accuracies: 76.0, 77.3, 75.3, 79.9, 71.4. These have a standard deviation of 3.1%. Don't forget to divide by 4 and not by 5; otherwise you will get a slightly smaller figure (2.8%).

L. The estimated mean converges to the true mean, and the estimated standard deviation converges to the true standard deviation. However, the n-1 factor in the denominator of the variance formula means that the standard deviation should decrease, although the effect is very small.

M. It's a tie, so ZeroR chooses one of the three classes as the the majority class and always predicts it, achieving a success rate of 50/150, or 33.3%.

N. Due to some statistical variation, you will receive an accuracy of 29% when using a percentage split of 66% and a random seed of 1 using ZeroR.

O. Using a percentage split of 66% on the glass dataset will give you 27% (not rounded: 27.3973%) using ZeroR.

P. J48 will give you a result of 58% (not rounded: 57.5342%) using a percentage split of 66% and a random seed of 1.

Q. When using NaiveBayes, you will get 49% accuracy using a percentage split of 66% and random seed of 1.

R. The majority class in the segment-challenge dataset is "path", which in the test set has 94 out of 810 instances. This results in 12% accuracy (not rounded: 11.6049%).

S. IBk has a much better performance than ZeroR, achieving 96% accuracy (not rounded: 95.8025%) on the test set.

T. PART is also much better than ZeroR, getting 96% accuracy (not rounded: 95.679%) on the test set.

U. 14-fold cross-validation gives 28%, and 10-fold cross-validation gives 33%.

V. it's not a coincidence. For each fold of the cross-validation, ZeroR chooses the majority class in the training set. Because there are a fixed number (50) of each class in the entire dataset, a class that is in the majority in the training set cannot be in the majority in the test set. Thus the success rate cannot possibly exceed 33% on any fold of the cross-validation.

W. With 5 folds the test set contains 30 instances; with 10 folds it contains 15; and with 25 folds it contains 6. These are all exact multiples of the number of classes (3). The number of instances in the training set is also an exact multiple of the number of classes in each case. Because Weka does stratified cross-validation, for each fold it is able to get an equal number of each class in both training and test sets. Thus the accurancy for each fold of the cross-validation is exactly 33.3%.

X. The dataset contains 150 instances, so 150-fold cross-validation sets aside exactly one for testing, and chooses the majority class of the remaining 149 training instances. That class will never be the same as the left-out instance's class, because the left-out class can never be the majority class in the training set.

Y. The random seeds generated the following accuracies: 96.1, 96.3, 94.5, 95.7, 96.1. This results in a mean of 95.7%.

Z. The random seeds generated the following accuracies: 96.1, 96.3, 94.5, 95.7, 96.1. This results in a standard deviation of 0.73%. (Note: you should divide by 4, not 5, otherwise you will get the smaller estimate of 0.65%.)

AA. Each 10-fold cross-validation involves running J48 11 times, once for each fold and once at the end, on the entire dataset. And you did this five times.

AB. The random seeds generated the following accuracies: 98.7, 95.3, 98.0, 96.0, 94.7. This results in a mean of 96.5%.

AC. The random seeds generated the following accuracies: 98.7, 95.3, 98.0, 96.0, 94.7. This results in a standard deviation of 1.73%. (Note: you should divide by 4, not 5, otherwise you will get the smaller estimate of 1.55%.)

AD. For each of the five runs Weka executes J48 twice, one on the 90% training set and again on the whole dataset (just as in cross-validation).

AE. A single rule outperforms ZeroR by a large margin on the "iris" dataset (92% vs 33%).

AF. The "Classifier model" output prints details of the model that is generated on the full training set. In case of OneR, it prints the rule.

AG. If the class distribution is skewed or limited data is available, predicting the majority class can yield better results than basing a rule on a single attribute. This happens with the nominal weather dataset

AH. OneR always outperforms (or, at worst, equals) ZeroR when evaluated on the training data. (Of course, evaluating on the training data doesn't reflect performance on independent test data.)

AI. If the dataset contains instances with the same attribute value but different classes, a rule based on that attribute will get one of them incorrect. This happens when temperature = 72, and when humidity = 70 and 90.

AJ. c.

AK. No.

AL. a.

AM. Cross-validating NaiveBayes with 10 folds on the "vote" dataset will give you an accuracy of 90% (not rounded: 90.1149%).

AN. By copying the "handicapped" attribute ten times, you get a dataset with 27 attributes on which NaiveBayes returns an accuracy of 89% (not rounded: 88.965%) when using 10-fold cross-validation.

AO. Using 20 copies of the "handicapped" attribute (37 attributes in the dataset altogether), results in an accuracy of 81% (not rounded: 80.9195%) for 10-fold cross-validation.

AP. Continuing to make copies of an attribute gives it an increasingly stronger contribution to the decision, until -- in the extreme -- the other attributes have no influence at all. You can determine this value by removing all attributes except "handicapped-infants" (and the class, of course).

AQ. The fundamental Naive Bayes assumption that each attribute makes an equal and independent contribution to the outcome may actually be correct! (In the present case, accuracy remains exactly the same when "handicapped-infants" is removed, so it seems that this attribute is irrelevant to the class.)

<p align="right">*Felix Dobslaw* - felix.dobslaw@miun.se</p>
