<center> <h1>Laboration 4: Weka Answers</h1> </center>
<center><h2>DT044A Data Mining<h2></center>
<center><h2>Mid Sweden University<h2></center>

### Answers to Questions in 1

A. Instances 71, 127 and 139 have the same attribute values, but their classes are versicolor, virginica and virginica respectively. The colors on the plot are mixed in the corresponding proportions.

B. 3. Logistic Regression uses a transformation technique called logit transformation, which assigns a classification a probability. The class belongingness is therefore not strict, but changes gradually on the borderline.

C. This is most striking for red, but the greenish and bluish colors are not pure either. The system is never completely certain about the classification of any point!

D. 4. Random forests are collections of trees, all slightly different.

E. Logistic regression is a sophisticated way of choosing a linear decision boundary for classification.

F. IBk's decision boundary in any localized region of instance space is linear, determined by the nearest neighbors of the various classes in that region. But the neighbors change when you move around instance space, so the boundary is a set of linear segments that join together. Support vector machines also produce piecewise linear boundaries.

G. J48 produces decision trees, which create non-linear boundaries in instance space.

H. The accuracies are: J48 = 70.5 < IBk = 72.0 < SMO = 75.1 ~ Logistic = 75.2.

I. IBk overfits dramatically: for this dataset it's 100% accurate on the training set! J48 can overfit badly because of the highly complex decision boundaries it can produce; the effect is ameliorated, but rarely completely eliminated, by its pruning algorithm. With this dataset, J48's apparent accuracy improves from 70.5% to 85.5%. Logistic regression is a sophisticated way of producing a good linear decision boundary, which is necessarily simple and therefore less likely to overfit; here its apparent performance increases just a little, from 75.5% to 78.6%. SMO can produce piecewise linear boundaries, but is resiient against overfitting because it relies on a small number of support vectors; gain its apparent performance increases just a little, from 75.1% to 78.4%.


J. The results are: ZeroR = 35.5% < NaiveBayes = 48.6% < Logistic = 64.0% < J48 = 66.8%.

K. The results are: Logistic = 0.24 < NaiveBayes = 0.26 < J48 = 0.47 < ZeroR = 0.48. Note that the root mean squared error should be minimized, not maximized, so the order in terms of increasing performance is ZeroR, J48, NaiveBayes, and Logistic.

L. The results are: Logistic = 68.9% < ZeroR = 70.3% < NaiveBayes = 71.7% < J48 = 75.5%. This reinforces the message that it always pays to check the baseline accuracy with ZeroR before choosing a sophisticated classifier like Logistic. Simplicity first!

N. The ranges are [0.854, 0.931] for Scheme 1, and [0.897, 0.961] for Scheme 2. We can improve it by increasing the number of folds, which results in a higher number of trials, N. Looking into the mean and standard deviation only, we are tempted to say that Scheme 2 is better. Statistically, we cannot back this up by a 10-fold Cross-Validation.

### Answers to Questions in 2

A. 30. These were all computer sales companies, in the old days.

B. Weka gives the result as 0.9257. However, the apparent accuracy is misleading, and it would be more realistic to round to a smaller number of significant digits, say 0.926 or -- even better -- 0.93.

C. Weka gives the result as 0.9766. However, the apparent accuracy is misleading, and it would be more realistic to round to a smaller number of significant digits, say 0.977 or -- even better -- 0.98.
  
D. You can see them in the classifier output, and also in the tree visualizer accessible from the right-click menu. The models are called LM 1 and LM 2.

E. The model produced by LinearRegression contains 11 attributes of the form "vendor = ...", and both linear models produced by M5P contain the same 3 attributes of this form.

F. 5. Try it! Note: be sure to select the "supervised" version of this filter, not the unsupervised one.

G. 3.

H. LinearRegression works with numeric classes, not with nominal ones.

I. There are 100 zero values and 50 one values, but it's a bit hard to determine that it's the virginica type that has been singled out. Use the "Edit" button to check the dataset before and after applying the filter -- the instance numbers don't change.

J. Weka gives the result as 0.7676. However, the apparent accuracy is misleading, and it would be more realistic to round to a smaller number of significant digits, say 0.768 or -- even better -- 0.77.

K. Hint: only two attributes (along with an offset) are used in the regression formula.

L. Weka gives the result as 0.458. However, the apparent accuracy is misleading, and it would be more realistic to round to a smaller number of significant digits, say 0.46.

M. Hint: The regression formula uses just the other three attributes (along with an offset).

N. Weka gives the result as 0.9456. However, the apparent accuracy is misleading, and it would be more realistic to round to a smaller number of significant digits, say 0.956 or -- even better -- 0.95.

O. Hint: The correlation coefficient for this class is much smaller than for the others.

P. For the three models that you have produced, the actual and predicted outputs for the third instance are: Iris-virginica: 0, 0.359; Iris-versicolor: 1, 0.322; and Iris-setosa: 0, 0.32. Thus the actual class is Iris-versicolor (because the actual class is a 1 in the second model), and the predicted class is Iris-virginica (because the first model predicts the largest output).

Q. Use the AddID unsupervised attribute filter, and the "Output additional attributes" option  from the Classifier panel "More options ..." menu. Be sure to use the attribute *index* (e.g., 1) rather than the attribute *name* (e.g., ID).

### Answers to Questions in 3

A. c. This data plots phone calls against year. In the sixties it exhibits a large, surprising, sustained, spike.

B. a.

C. b. Linear Regression fits a straight line to the data, which in this case is increasing. But it's not a very good fit. Imagine the regression line superimposed on the plot of phone calls against year. (Unfortunately you will have to imagine it, because Weka's visualization facilities do not allow two graphs to be plotted at once.)

D. a.

E. They're very different: the maximum value for the LinearRegression attribute is 10.8, whereas the maximum value for LeastMedSq is only 2.7. To see the difference graphically, sketch them on a piece of paper using the same scale on the vertical axis. Also, imagine the LeastMedSq line superimposed on the plot of phone calls against year, as you did earlier for LinearRegression.

F. The correlation of the predictions with the data has improved enormously, from 0.45 to 0.99 -- near-perfect correlation.

G. Instead of the number of phone calls, between 1964-1970 the total duration of the calls (in minutes) was recorded. Such errors are quite common in practice.

H. Weka gives the result as 73.6842. However, the apparent accuracy is misleading, and it would be more realistic to round to a smaller number of significant digits, say 73.7 or -- even better -- 74.

I. Six attributes remain, excluding the class. All of the original attributes (apart from the class) have missing values. However, only 10 have at least 33% of missing values. You can see this by selecting individual attributes in the Preprocess panel.

J. Weka gives the result as 80.7018. However, the apparent accuracy is misleading, and it would be more realistic to round to a smaller number of significant digits, say 80.7 or -- even better -- 81.

K. 2. It replaces missing values in numerical attributes by the average value, and replaces missing values in nominal attributes by the mode, i.e., the most popular value.

L. Weka gives the result as 80.7018. However, the apparent accuracy is misleading, and it would be more realistic to round to a smaller number of significant digits, say 80.7 or -- even better -- 81.
This is the same as with the reduced attribute set. However, the confusion matrix is different. In general, it's better to replace missing values rather than delete them entirely, since in many cases these attributes will contribute some useful information.

M. 2. There's a rather subtle (and almost certainly insignificant) problem. The means and modes are calculated over the whole dataset. Thus for each fold of the cross-validation, some of the attribute values in the training set have been contaminated with information from the test set (although the effect is probably very small). This could produce results that are slightly different from those obtained from a completely independent test set in which missing values are replaced by means/modes from that test set.

N. 1.

O. 1.

P. 1.

Q. 1.

R. Thelma Arnold. You can read about this fiasco in many places, for example, the Wikipedia article entitled "AOL search data leak".

<p align="right">*Felix Dobslaw* - felix.dobslaw@miun.se</p>
