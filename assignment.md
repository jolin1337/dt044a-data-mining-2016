<center> <h1>Assignment: Data Mining on real world dataset</h1> </center>
<center><h2>DT044A Data Mining<h2></center>
<center><h2>Mid Sweden University<h2></center>

### Motivation
The company you work for, **BigDataMonkeyInc**, markets itself with buzz-words such as *Big Data*, *Business Intelligence*, and *Data Science*. BigDataMonkeyInc claims that if there is any undiscovered value in your data, they will find it. You are new on the job, and this is (possibly) your first Data Mining assignment. A large online sports betting site got in touch with BigDataMonkeyInc. They have existing machine learning algorithms, and a well functioning broker software. However, the competition is tough, and many new small enterprises push forward into the market. They don't know exactly how they can improve their position in the competition, but believe that the answer lies in the data. Their objective with hiring you is to reveal interesting patterns in the data, that can potentially help them increase market shares. They gave this assignment to three competing data science companies, and leave you with the one briefing "all significant findings are of importance". This can range from the revelation of simple patterns, to the recommendation of a machine learning algorithm that predicts the outcome of games. Their approach is to intentionally not tell you anything specific about the data. They say they won't answer questions on how the data is used today in order to allow you an entirely new view on the data that they stared themselves blind on.

### Approach
This assigment combines all the knowledge you have gathered throughout the laborations. The [*soccer databaset*](https://bitbucket.org/feldob/soccer-dataset/raw/master/footballStats.sql) (as we call it from here on) is the main dataset we use for most laborations. There are no other mandatory laboration assignments in the course that have to be handed in; all other briefings are ment to prepare you for this assignment, your project, and the final exam.

### Requirements
You are allowed and encouraged to work in teams of not more than two students. The submission must follow the provided LaTeX [template](https://bitbucket.org/feldob/dt044a-data-mining-2016/raw/master/reportTemplateDataMining.tex); the same template as for the final course project. You are allowed to use Microsoft Word or LibreOffice, as long as you follow the same structure as in the template (find the template pdf [here](https://bitbucket.org/feldob/dt044a-data-mining-2016/raw/master/reportTemplateDataMining.pdf)).
* You use either R or Weka or a combination of both for the analysis, and attach all code in the appendix of the submission.
* Instead of the 15 pages in the project report template for chapter 3, your target is 8 pages.

### Deliverables
The report as a pdf submission through the Assignment Hand-In on Moodle.

### Hints
The report is not a classical report. In Data Mining, not only the result, but also and especially *the way is the goal*. Therefore, in chapter 3, you will have to explain your approach by outlining the steps that were required to reach your conclusions; even intermediate ones. For instance, in order to analyse a certain variable, you had to transform it from being nominal to numeric. Or, you discovered that no information could be gained by doing X, but when extending that including variable Z, a significant relationship could be verified. You can see it as a diary of the most relevant activities that lead you from conlusion to conclusion. Of ominent importance is that you provide the evidence that you base your conclusions on, in order to make the steps reproducible.

The deadline of the assignment can be found on the course board. Please don't submit unfinished assignments. If you struggle with a task or have questions, ask them in the laboration sessions or on the course board so that we can sort things out together.

Have fun!

<p align="right">*Felix Dobslaw* - felix.dobslaw@miun.se</p>
