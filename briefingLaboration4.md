<center> <h1>Laboration 4: Data Preparation and Transformation</h1> </center>
<center><h2>DT044A Data Mining<h2></center>
<center><h2>Mid Sweden University<h2></center>

In lecture 4, you were presented with data preparation techniques concerning attribute selection, transformation, and data sampling, projection as well as cleansing. In this laboration, you will collect practical experience on preparing your data in order to get the most out of it. There is no right or wrong order in which to do the exercises. None of the results are supposed to be submitted.

### 1. Additional Evaluation Exercises

Open the BoundaryVisualizer tool, which is located in the Visualization menu of the small "Chooser" panel that appears when you start Weka.

Load the iris.2D dataset and check Plot training data.

A. Choose the IBk classifier with default options and start the boundary visualization. You will notice a small, faint region of mixed color (green and blue). How can you get mixed colors when only one nearest neighbor is being used? Hint: examine the points in this small region using the Explorer's Visualize panel.

1. There are instances with different classes at the same point of the graph
2. IBk has a bug
3. IBk defaults to 2 nearest neighbors
4. IBk defaults to more than 2 nearest neighbors

B. Look at the decision boundary created by the Logistic Regression method. You will find it at functions > Logistic. What can you say about the boundary shapes?

1. The boundaries are non-linear
2. Crisp boundaries, changing abruptly from one color to another
3. Gradual transition from one color to another
4. Checkered pattern, crisp boundaries, no mixed colors

C. Look at the decision boundary created by the Support Vector Machine method. You will find it at functions > SMO. What can you say about the resulting plot?

1. The boundaries are non-linear
2. There are no areas of pure color
3. Checkered pattern with crisp boundaries
4. Checkered pattern with slightly fuzzy boundaries

D. Look at the decision boundary created by the Random Forest method. You will find it at trees > RandomForest. What can you say about the boundary shapes?

1. Crisp boundaries, changing abruptly from one color to another
2. There are no areas of pure color
3. Gradual transition from one color to another
4. Checkered pattern with slightly fuzzy boundaries


E. Which of the following machine learning methods produce decision boundaries that are strictly linear?

1. IBk
2. J48
3. Logistic regression
4. SMO (Support vector machines)

F. Which of the following machine learning methods produce decision boundaries that are piecewise linear?

1. IBk
2. J48
3. Logistic regression
4. SMO (Support vector machines)

G. Which of the following machine learning methods produce decision boundaries that are definitely nonlinear?

1. IBk
2. J48
3. Logistic regression
4. SMO (Support vector machines)

H. Open the credit-g dataset, run the four algorithms IBk, J48, Logistic, and SMO, and record the accuracy using 10-fold cross-validation. What order do the algorithms come in?

1. IBk < Logistic < SMO < J48
2. Logistic < IBk < J48 < SMO
3. J48 ~ Logistic < SMO ("~" means about the same)
4. J48 < IBk < SMO ~ Logistic

I. As you know, performance estimates obtained on the training set are overly optimistic because of overfitting. Just how badly algorithms overfit can be judged in terms of the apparent performance improvement from cross-validation to training-set evaluation. Given what you know about how they operate, order these four algorithms according to the expected amount of overfitting, from greatest to least. Then confirm your intuition with a Weka experiment using the credit-g dataset.

1. IBk ... J48 ... SMO ~ Logistic
2. IBk ... Logistic ... SMO ... J48
3. Logistic ... IBk ... J48 ... SMO
4. J48 ~ Logistic ... SMO ... IBk

J. Open the glass dataset and run ZeroR, NaiveBayes, J48 and Logistic, in each case recording the accuracy using 10-fold cross-validation. Which is the correct ordering in terms of accuracy?

1. ZeroR < Logistic < NaiveBayes < J48
2. NaiveBayes < ZeroR < Logistic < J48
3. ZeroR < NaiveBayes < Logistic < J48
4. ZeroR < NaiveBayes < J48 < Logistic

K. Repeat the above using the labor dataset, the same four algorithms (ZeroR, NaiveBayes, J48, Logistic), and 10-fold cross-validation. In each case record the root mean squared error. Which is the correct ordering in terms of this measure?

1. J48 < Logistic < NaiveBayes < ZeroR
2. Logistic < NaiveBayes < J48 < ZeroR
3. ZeroR < NaiveBayes < J48 < Logistic
4. Logistic < J48 < NaiveBayes < ZeroR

L. Repeat the above using the breast-cancer dataset, the same four algorithms (ZeroR, NaiveBayes, J48, Logistic), and 10-fold cross-validation. In each case record the accuracy. Which is the correct ordering in terms of accuracy?

1. ZeroR < NaiveBayes < J48 < Logistic
2. ZeroR < NaiveBayes < Logistic < J48
3. ZeroR < Logistic < NaiveBayes < J48
4. Logistic < ZeroR < NaiveBayes < J48


M. Suppose the following table presents two samples of mean error rates (in %) obtained by 10-fold Cross-Validation using two different learning schemes on the same datasets. Find out if one scheme is better given a confidence limit of 20%.

* Why is the confidence range so wide? How to improve it?
* How is the result if we directly calculated the mean variance, and standard deviation? Can we rely on those numbers?

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:0px;overflow:hidden;word-break:normal;}
.tg .tg-e3zv{font-weight:bold}
</style>
<table class="tg">
  <tr>
    <th class="tg-e3zv">Scheme 1<br></th>
    <th class="tg-031e">10</th>
    <th class="tg-031e">11</th>
    <th class="tg-031e">12</th>
    <th class="tg-031e">8</th>
    <th class="tg-031e">9</th>
    <th class="tg-031e">10</th>
    <th class="tg-031e">10</th>
    <th class="tg-031e">9</th>
    <th class="tg-031e">11</th>
    <th class="tg-031e">11</th>
  </tr>
  <tr>
    <td class="tg-e3zv">Scheme 2<br></td>
    <td class="tg-031e">5</td>
    <td class="tg-031e">6</td>
    <td class="tg-031e">10</td>
    <td class="tg-031e">9</td>
    <td class="tg-031e">5</td>
    <td class="tg-031e">6</td>
    <td class="tg-031e">6</td>
    <td class="tg-031e">5</td>
    <td class="tg-031e">6</td>
    <td class="tg-031e">6</td>
  </tr>
</table>

N. Given the following table, suppose that a customer analysis leads to the conclusion that the probability of a response from a certain customer is:</br>

<center>
*P(person) = 100 - person.age*
</center>

* Write down the lift table given the percentage measure.
* What is the lift fact of the 10% (20%) sample in the above lift table?

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#669;background-color:#e8edff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#039;background-color:#b9c9fe;}
.tg .tg-lqy6{text-align:right;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-yw4l">Customer Name</th>
    <th class="tg-yw4l">Height</th>
    <th class="tg-yw4l">Age</th>
    <th class="tg-lqy6">Response</th>
  </tr>
  <tr>
    <td class="tg-yw4l">Alan</td>
    <td class="tg-yw4l">70</td>
    <td class="tg-yw4l">39</td>
    <td class="tg-lqy6">N</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Bob</td>
    <td class="tg-yw4l">72</td>
    <td class="tg-yw4l">21</td>
    <td class="tg-lqy6">Y</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Jessica</td>
    <td class="tg-yw4l">65</td>
    <td class="tg-yw4l">25</td>
    <td class="tg-lqy6">Y</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Elizabeth</td>
    <td class="tg-yw4l">62</td>
    <td class="tg-yw4l">30</td>
    <td class="tg-lqy6">Y</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Hilary</td>
    <td class="tg-yw4l">67</td>
    <td class="tg-yw4l">19</td>
    <td class="tg-lqy6">Y</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Fred</td>
    <td class="tg-yw4l">69</td>
    <td class="tg-yw4l">48</td>
    <td class="tg-lqy6">N</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Alex</td>
    <td class="tg-yw4l">65</td>
    <td class="tg-yw4l">12</td>
    <td class="tg-lqy6">Y</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Margot</td>
    <td class="tg-yw4l">63</td>
    <td class="tg-yw4l">51</td>
    <td class="tg-lqy6">N</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Sean</td>
    <td class="tg-yw4l">71</td>
    <td class="tg-yw4l">65</td>
    <td class="tg-lqy6">Y</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Chris</td>
    <td class="tg-yw4l">73</td>
    <td class="tg-yw4l">42</td>
    <td class="tg-lqy6">N</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Philip</td>
    <td class="tg-yw4l">75</td>
    <td class="tg-yw4l">20</td>
    <td class="tg-lqy6">Y</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Catherine</td>
    <td class="tg-yw4l">70</td>
    <td class="tg-yw4l">23</td>
    <td class="tg-lqy6">N</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Amy</td>
    <td class="tg-yw4l">69</td>
    <td class="tg-yw4l">13</td>
    <td class="tg-lqy6">N</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Erin</td>
    <td class="tg-yw4l">68</td>
    <td class="tg-yw4l">35</td>
    <td class="tg-lqy6">Y</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Trent</td>
    <td class="tg-yw4l">72</td>
    <td class="tg-yw4l">55</td>
    <td class="tg-lqy6">N</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Preston</td>
    <td class="tg-yw4l">68</td>
    <td class="tg-yw4l">25</td>
    <td class="tg-lqy6">N</td>
  </tr>
  <tr>
    <td class="tg-yw4l">John</td>
    <td class="tg-yw4l">64</td>
    <td class="tg-yw4l">76</td>
    <td class="tg-lqy6">N</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Nancy</td>
    <td class="tg-yw4l">64</td>
    <td class="tg-yw4l">24</td>
    <td class="tg-lqy6">Y</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Kim</td>
    <td class="tg-yw4l">72</td>
    <td class="tg-yw4l">31</td>
    <td class="tg-lqy6">N</td>
  </tr>
  <tr>
    <td class="tg-yw4l">Laura</td>
    <td class="tg-yw4l">62</td>
    <td class="tg-yw4l">29</td>
    <td class="tg-lqy6">Y</td>
  </tr>
</table>


### 2. Attribute Filters and Transformation

A. In the Explorer, open the cpu dataset and examine its attributes; notice that they are all numeric. Now open the cpu.with.vendor dataset. It's exactly the same except for one additional attribute, which is nominal. How many different values does that attribute have?

1. 4
2. 8
3. 30
4. 32
5. 209

B. Run the LinearRegression classifier (with default options) on the cpu.with.vendor dataset, using 10-fold cross-validation. Several different measures are printed. Linear regression optimizes the root mean squared error; small is good. Instead we will look at the correlation coefficient. Large correlation is good, and the value cannot be greater than 1. What is the correlation coefficient in this case?

C. Now find the M5P tree-based algorithm and run it (with default options), again using 10-fold cross-validation. What is the correlation coefficient now?

D. M5P produces a tree with linear models at the leaves. How many linear models are there?

1. 1
2. 2
3. 4
4. 10
5. 209

E. The single nominal attribute in this dataset is called "vendor". Both LinearRegression and M5P convert it into several binary attributes, with values 0 and 1, that are used in the weighted summation produced by the regression method. How many of these binary attributes, all derived from "vendor", are used by LinearRegression and M5P respectively?

1. Just one in each case
2. 1 and 2 for LinearRegression and M5P respectively
3. 8 and 2 respectively
4. 11 and 3
5. 11 and 6
6. 30 in each case

F. Weka has a supervised attribute filter that converts a nominal attribute into the same set of binary attributes used by LinearRegression and M5P. What is it called?

1. AddClassification
2. AttributeSelection
3. ClassOrder
4. Discretize
5. NominalToBinary
6. PLSFilter


G. There is an unsupervised attribute filter with the same name. This also converts a nominal attribute into a set of binary attributes, but in a different way. What does it do?

1. Produces a smaller number of binary attributes
2. Produces a larger number of binary attributes
3. Creates one binary attribute for each value of the nominal attribute
4. Uses larger value sets for each binary attribute
5. Uses different value sets for each binary attribute

Open the iris dataset in the Explorer.

H. In the Classify panel, check the LinearRegression classifier. You will find that it is grayed out. Why?

1. The attributes in the dataset are all numeric
2. There are not enough attributes in the dataset
3. Linear regression only works in New Zealand
4. The class is not numeric

I. Apply the MakeIndicator filter (which is unsupervised), with default parameters. How does it affect the class value?

1. Leaves it unchanged
2. Makes it 0 for Iris-setosas and 1 for the other iris types
3. Makes it 1 for Iris-setosas and 0 for the other types
4. Makes it 0 for Iris-versicolors and 1 for the other types
5. Makes it 1 for Iris-versicolors and 0 for the other types
6. Makes it 0 for Iris-virginicas and 1 for the other types
7. Makes it 1 for Iris-virginicas and 0 for the other types

Run the LinearRegression classifier on the filtered dataset, using 10-fold cross-validation. But first, be sure to enable Output predictions in the More options menu. You will need these predictions later on.

J. What is the correlation coefficient?

K. Which attribute or attributes are used in the model created for the full dataset?

1. all of them
2. just sepalwidth
3. just petallength
4. sepallength and petallength
5. sepalwidth and petalwidth

Undo your changes in the Preprocess panel, and investigate the effect of the filter's valueIndices parameter. Use it to make a class value that is 1 only for Iris-versicolors. Run LinearRegression again, with the same settings.

L. What is the correlation coefficient?

M. Which attribute is not used in the model created for the full dataset?

1. sepallength
2. sepalwidth
3. petallength
4. petalwidth

Now repeat the above for the Iris-setosas.

N. What is the correlation coefficient?

O. The Multiresponse linear regression method will choose the class of an instance according to whichever of the three regression formulae produces the largest output. Judging by the correlations you have observed, which class do you think it will produce most errors for?

1. Iris-setosa
2. Iris-versicolor
3. Iris-virginica
4. petalwidth

P. Now look at the predictions that are output by the three models for the first four instances only. Weka outputs predictions in the shuffled order that is used by the cross-validation, not in the instances' original order -- which makes it hard to determine their true class. But the information is there. Multiresponse linear regression will make just one error on the first four instances (of the total of 150 instances used in the 10-fold cross-validation). Which instance is it?

1. first
2. second
3. third
4. fourth

Q. As you saw in the last question, Weka outputs predictions in shuffled order. This can be a nuisance. However, a filter and an additional Classify panel option can be used to show the original instance numbers alongside the predictions. What filter, and what option?

[Warning: if you use this Classify panel option, note that the attribute should be specified as a number, not a name. In Weka 3.6.++, Weka hangs if you specify a non-numeric value; this bug has been fixed in later versions.]


### 3. Outliers, Missing Values and Correlation

In this activity you will undertake a very simple real-world data mining problem, look at the data, find out what's wrong with it, and correct it.

Download the [regression_outliers](https://dl.dropboxusercontent.com/u/35363454/datamining/data/regression_outliers.csv) dataset and open it with Weka. This is a .csv format file (comma-separated values, a common spreadsheet file format). Weka can open such files directly: you need to change the File Format on the Open file dialog box.

A. View the data in Weka's Visualize panel. What does it look like?

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/phone_stats.png" alt="phone stats" style="width:400px"/>
</center>

B. The class (phone calls) is numeric, so LinearRegression seems appropriate. Try it. What is the correlation coefficient, when evaluated with 10-fold cross-validation?

1. 0.45
2. 0.55
3. 0.95
4. 0.99
5. 1.00

C. In order to visualize the result, add the classifier's predictions to the dataset using the supervised filter AddClassification, and go to the Visualize panel. (As you will discover, this filter does nothing unless you turn on the outputClassification option.) What does the plot look like?

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/addClassification.png" alt="classification output" style="width:400px"/>
</center>

D. Change the classifier to LeastMedSq, another regression-style classifier, add its predictions to the dataset, and visualize the result. (If you want to retain the attribute added in the last question, save your data file, change the attribute name with a text editor, and reload it.) What does the plot look like?

<center>
<img src="https://dl.dropboxusercontent.com/u/35363454/datamining/leastMedSq.png" alt="LesatMedSq" style="width:400px"/>
</center>

LeastMedSq gives an accurate regression line even when there are outliers. However, it is computationally very expensive. In practical situations it is common to delete outliers manually and then use LinearRegression.

E. Do the LinearRegression and LeastMedSq methods yield similar regression lines?

1. yes
2. no

LeastMedSq gives an accurate regression line even when there are outliers. However, it is computationally very expensive. In practical situations it is common to delete outliers manually and then use LinearRegression.

F. Identify the outliers using the Visualize panel and remove them manually (there are six obvious ones, plus two more that are not quite so striking). Apply linear regression again. What is the correlation coefficient, when evaluated with 10-fold cross-validation?

1. 0.45
2. 0.55
3. 0.95
4. 0.99
5. 1.00

G. There's something fishy about this data. Do an internet search for "International Phone Calls from Belgium 1950-1973" and find out what went wrong with the dataset. What was the reason for the outliers?

1. Human error
2. Different measurement used
3. Flat rate for calls introduced

Open the labor dataset and go to the Classify tab.

H. Select the J48 classifier (default options). What is its accuracy, evaluated using 10-fold cross-validation?

Return to the Preprocess tab and remove all attributes with 33% or more missing values.

I. How many attributes are left?

1. 3
2. 6
3. 12
4. 17

J. Re-run J48 under the same conditions as before. What accuracy is achieved now?

K. Now revert to the original dataset and apply the ReplaceMissingValues filter. This filter replaces missing values. What does it replace them with?

1. Blank for nominal attributes and zero for numeric attributes
2. Mean for numeric attributes and mode for nominal attributes
3. Random values for both nominal and numeric attrbutes
4. Mean for nominal attributes and mode for numeric attributes

L. What is J48's accuracy now?

M. Can you spot a methodological problem with this experiment?

1. No
2. Yes

Suppose you observe from your data mining activities that events A and B are correlated, but you have no further information about them.
Which of the following statements might be true?

N. A causes B.

1. True
2. False

O. B causes A.

1. True
2. False

P. Some third factor C is the cause of both A and B.

1. True
2. False

Q. The correlation between A and B is entirely coincidental.

1. True
2. False

R. In 2006, a text file was released on the web containing 20,000,000 search engine queries made by 650,000 users over a 3-month period, intended for research purposes. The file had been anonymized by replacing user names with random numbers, one per user. However, some of the queries contained clues to the user's identity. The New York Times was able to locate an individual from these supposedly anonymized search records by cross referencing them with phonebook listings. Look up this renowned example of reidentification and read about it. What is the name of the user identified by the New York Times?

<p align="right">*Felix Dobslaw* - felix.dobslaw@miun.se</p>
