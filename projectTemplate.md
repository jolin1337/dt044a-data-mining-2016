<center>
<h1> DT044A Data Mining 2016 Project Description </h1>
<h2> PROJECT NAME </h2>
</center>

### Participants

List your student id's here.

### Brief Background

Explain what problem scope you would like to address, and why.

### Questions

What are the concrete questions which you would like to find answers to?
1. question 1
2. question 2
3. ...

### Data Sources

List those data sources from [Transport Analys](http://trafa.se/en/), and other external sources, which you would like to use/combine
* data source 1
* data source 2
* ...

### Preferred Date for Presentation

[ ] 3/3

[ ] 9/3

[x] does not matter

