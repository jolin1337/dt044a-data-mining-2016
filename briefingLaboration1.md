<center> <h1>Laboration 1: Explore</h1> </center>
<center><h2>DT044A Data Mining<h2></center>
<center><h2>Mid Sweden University<h2></center>

For each of the eight laboration session you will receive a briefing like this. The exercises are supposed to help you to gather the knowledge required to address the compulsory tasks in the course. This briefing focuses on getting you started for the course.

In your first laboration, you will get to know the [**soccer dataset**](https://bitbucket.org/feldob/soccer-dataset/raw/4467fbd90aee9e7dcd5b1594a6015dc7f24d7265/footballStats.sql). The soccer dataset is a SQL database of ~20 MB that we are going to spend much of our laboratory time on. In order to get hold of it, sign up on Bitbucket with your student email address, and request access to the resource by sending me an email to felix.dobslaw@miun.se. I will grant you access and confirm your mail. From then on, you can access the dataset.
Get started by importing the dataset according to the instructions in [here](https://bitbucket.org/feldob/soccer-dataset).

There is no right or wrong order in which to do the exercises. None of the results are supposed to be submitted.

## 1) Course Assignment
The single assignment for the course is based on the Soccer dataset. Find the assignment [here](https://bitbucket.org/feldob/dt044a-data-mining-2016/raw/master/assignment.md). To get started, explore the data and play with it:

1. How many tables are in the database?
2. Draw a diagram of the relationships in the database.
3. For what timespan is the data valid?
4. What leagues are investigated?
5. What information about players is available?
6. Why should data analysis never modify the original data but create a copy of it to conduct transformations, joining, filters etc.?
7. What are attribute type and range for the following database columns:
	1. scores.home
	2. games.scorehome
	3. players.name
	4. matchdays.gameofseason 
	
	Do you assume the closed or open world assumption?
	
7. Create a view that contains a transformation column 'result' with input range {*WIN*, *DRAW*, *LOSS*}, that reflects the result from the perspective of the home team.
8. Find out what a [**materialized view**](http://www.postgresql.org/docs/current/static/sql-creatematerializedview.html) is, and why it might be a valuable feature in the context of data mining.
9. Can you identify a potential stakeholder and question for the dataset?
	a. What columns (or *attributes*) and which rows (or *instances*) in the database may be of interest for the analysis.
	b. Build a valid **input** in the form that data mining algorithms can work with.

## 2) Project Choice
The project task is to conduct an extended explorative data analysis on open datasets concerning **Transportation and Traffic**. Find the template in [here](https://bitbucket.org/feldob/dt044a-data-mining-2016/raw/master/reportTemplateDataMining.tex).

1. Find a partner for the project.
2. Start exploring the different datasets. Reason about the topic you would like to address.
3. Create a first draft version of the project description.

<p align="right">*Felix Dobslaw* - felix.dobslaw@miun.se</p>
