# DT044A Data Mining: Project Registration

The project accounts for 3.5 hp of the course. The project description weights into the grading.  You sign up for the project as follows

1.  download [this template](https://bitbucket.org/feldob/dt044a-data-mining-2016/raw/master/projectTemplate.md)
2.  rename it to your "STUDID\_dm16.md" (for groups of 2, concatenate your STUDID by a score '-')
3. fill in the 4 categories
	* brief background information
	* questions to address
	* used data sources (at least 2 from [transport analys](http://trafa.se/en/))
	* preferred date for presentation

4. submit the file via the *Project Description Hand-In* assignment box (each student, not one per project)

Changes/extensions in the choice of data sources or questions in the report due to your findings are allowed. Still, in those cases you will have to explain/justify them in your report.

Each accepted description will be confirmed with a time slot for the presentation.

<p align="right">*Felix Dobslaw* - felix.dobslaw@miun.se</p>
